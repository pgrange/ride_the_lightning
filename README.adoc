ride the lightning ansible role
===============================

Installs Ride The Lightning (RTL) to manage a local eclair lightning node, see [eclair](http://gitlab.com/pgrange/eclair) for an ansible role to install it.

Ride the lightning will listen on port 3000.

configuration
-------------

You must indicate a password to access Reide The Lightning. See ansible vault to securely store secrets.

- rtl_password: password used to secure access to ride the lightning.

Elements that can be configured for now:

- rtl_eclair_group: the system group that rtl must belong to so that it can access eclair config file with api password.

tests
-----

Tests are making usage of bash_unit and role_unit. They can be launched using the following command line:

----
tests/bash_unit tests/tests_*
----
